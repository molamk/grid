import { INTEGER, STRING } from "sequelize";
import { database } from "../lib/repository/database";
import { IDatabaseSchema, IModel } from "../lib/repository/schema";
import { generateSalt, hashPassword } from "../lib/utils/hashing";
import { logger } from "../lib/utils/logger";
import { IUser } from "./user";

export interface IUser {
  id?: number;
  email: string;
  password: string;
  salt?: string;
  createdAt?: string;
  updatedAt?: string;
}

export const userSchema: IDatabaseSchema = {
  name: "user",
  options: {
    email: { type: STRING, unique: true, allowNull: false },
    id: {
      autoIncrement: true,
      primaryKey: true,
      type: INTEGER,
    },
    password: { type: STRING, allowNull: false },
    salt: { type: STRING, allowNull: false },
  },
};

const hashPasswordHook = (user: IUser) => {
  const salt = generateSalt();
  logger.info(`Salt: ${salt}`);
  user.salt = salt;
  user.password = hashPassword(user.password, salt);
};

export const userModel: IModel<IUser> = database.getModel(userSchema, {
  beforeValidate: hashPasswordHook,
});
