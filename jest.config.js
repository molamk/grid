module.exports = {
    "verbose": true,
    // "roots": [
    //     "<rootDir>/src"
    // ],
    "transform": {
        "^.+\\.(ts|tsx)$": "ts-jest"
    },
    "testRegex": "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
    "moduleFileExtensions": [
        "ts",
        "tsx",
        "js",
        "jsx",
        "json",
        "node"
    ],
    "coverageDirectory": "coverage",
    "collectCoverageFrom": ["src/**/*.ts"],
    "collectCoverage": true,
    "coverageReporters": [
        "json",
        "lcov",
        "text"
    ],
    "coverageThreshold": {
        "global": {
            "branches": 100,
            "functions": 100,
            "lines": 100,
            "statements": 100
        }
    },
    "globals": {
        "ts-jest": {
            // "tsConfigFile": "tsconfig.json"
            disableSourceMapSupport: true,
        }
    },
    "testEnvironment": 'node'
}