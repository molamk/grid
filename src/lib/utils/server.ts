import express from "express";
import { IEndpointInstance } from "../endpoint/endpoint";
import { IObservableExpress } from "../http/express-utils";
import { IHttpRequest, IHttpResponse } from "../http/http";

interface IWithStartTime {
  startTime: number;
  // url: string;
  endpoint: IEndpointInstance;
  request: IHttpRequest;
  // response: express.Response;
}

interface IAfterFlatten {
  response: express.Response;
  payload: IHttpResponse;
  startTime: number;
}

export interface IReqEndpoint {
  endpoint: IEndpointInstance;
  request: IHttpRequest;
}

export const injectStartTime = (x: IObservableExpress): IWithStartTime => ({
  endpoint: x.endpoint,
  request: x.request,
  startTime: Date.now(),
});

export const processRequest = (x: IWithStartTime) =>
  new Promise<{
    // response: express.Response;
    payload: IHttpResponse;
    // startTime: number;
  }>((resolve, reject) => {
    x.endpoint(x.request)
      .then((payload) => {
        resolve({
          payload,
          // response: x.response,
          // startTime: x.startTime,
        });
      })
      .catch((err) => reject(err));
  });

export const injectDuration = (x: IAfterFlatten) => ({
  duration: Date.now() - x.startTime,
  payload: x.payload,
  response: x.response,
});

// export const injectError = (
//   err: any,
//   x: IWithStartTime,
// ): Rx.Observable<IAfterFlatten> =>
//   Rx.of({
//     ...x,
//     payload: {
//       message: `${err}`,
//       statusCode: 400,
//     },
//   });

export interface ITmpWithTime {
  startTime: number;
  endpoint: IEndpointInstance;
  request: IHttpRequest;
}
