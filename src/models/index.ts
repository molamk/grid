import { IDatabaseSchema } from "../lib/repository/schema";
import { userSchema } from "./user";

export const allSchemas: IDatabaseSchema[] = [userSchema];
