import { IHttpRequest, IHttpResponse } from "../http/http";

export type IEndpointInstance = (req: IHttpRequest) => Promise<IHttpResponse>;

export interface IEndpointDefinition<X, Y> {
  decoder: (req: IHttpRequest) => X;
  processor: (req: X) => Promise<Y>;
  encoder: (res: Y) => IHttpResponse;
}

export function generateEndpoint<X, Y>(
  endpointParams: IEndpointDefinition<X, Y>,
): IEndpointInstance {
  return (input: IHttpRequest): Promise<IHttpResponse> => {
    const decoded = endpointParams.decoder(input);
    const handled = endpointParams.processor(decoded);
    return new Promise((resolve, reject) => {
      handled
        .then((h) => {
          const encoded = endpointParams.encoder(h);
          resolve(encoded);
        })
        .catch((err) => reject(err));
    });
  };
}
