import { createLogger, format, Logger, transports } from "winston";
const { combine, timestamp, colorize, printf, prettyPrint } = format;

class LoggerSingleton {
  public static getInstance() {
    if (!LoggerSingleton.instance) {
      LoggerSingleton.instance = new LoggerSingleton();
    }
    return LoggerSingleton.instance;
  }

  private static instance: LoggerSingleton;
  public logger: Logger;

  private constructor() {
    const myFormat = printf((info) => {
      return `${info.timestamp} ${info.level} ${info.message}`;
    });

    this.logger = createLogger({
      format: combine(colorize(), timestamp(), prettyPrint(), myFormat),
      transports: [new transports.Console()],
    });
  }
}

export const logger = LoggerSingleton.getInstance().logger;
