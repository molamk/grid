import * as bodyParser from "body-parser";
import express from "express";
import * as ExpressUserAgent from "express-useragent";
import morgan from "morgan";
import * as Rx from "rxjs";
import { IEndpointInstance } from "../endpoint/endpoint";
import { IExpressRoute } from "../routes/route";
import { formatExpressRequest, getFormattedURL } from "./builder";
import { DEFAULT_HTTP_PORT } from "./constants";
import { IHttpRequest } from "./http";

export interface IObservableExpress {
  url: string;
  endpoint: IEndpointInstance;
  request: IHttpRequest;
  response: express.Response;
}

export const getExpressApp = (
  port: number = DEFAULT_HTTP_PORT,
): express.Express => {
  const app: express.Express = express();
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.set("port", port);
  app.set("trust proxy", true);
  app.use(ExpressUserAgent.express());
  app.use(morgan("combined"));

  return app;
};

export const getExpressObsevablesList = (
  expressApp: express.Express,
  routes: IExpressRoute[],
) => {
  return routes.map((route) => {
    const subject = new Rx.Subject<IObservableExpress>();
    const formattedURL = getFormattedURL(route);

    expressApp[route.method](formattedURL, (request, response) =>
      subject.next({
        endpoint: route.endpoint,
        request: formatExpressRequest(request, route),
        response,
        url: formattedURL,
      }),
    );
    return subject;
  });
};
