import { generateSalt, SALT_LENGTH } from "./hashing";

test("it should generate different salts", () => {
  const salts = Array(10)
    .fill(0)
    .map(() => generateSalt());
  const unique = [...new Set(salts)];
  expect(salts.length).toBe(unique.length);
  salts.forEach((s) => expect(s.length).toBe(SALT_LENGTH));
});
