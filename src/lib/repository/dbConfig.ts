export interface IDatabaseConfig {
  name: string;
  username: string;
  password: string;
  host: string;
}

export const DEFAULT_DB_CONFIG: IDatabaseConfig = {
  host: "localhost",
  name: "grid",
  password: "root",
  username: "root",
};
