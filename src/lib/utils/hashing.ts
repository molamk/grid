import crypto from "crypto";

export const SALT_LENGTH = 16;
const HASHING_ALGORITHM = "sha512";

export const generateSalt = (): string =>
  crypto
    .randomBytes(Math.ceil(SALT_LENGTH / 2))
    .toString("hex")
    .slice(0, SALT_LENGTH);

export const hashPassword = (pass: string, salt: string): string => {
  const hash = crypto.createHmac(HASHING_ALGORITHM, salt);
  hash.update(pass);
  return hash.digest("hex");
};
