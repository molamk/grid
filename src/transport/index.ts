// import express from "express";
// import http from "http";
// import * as Rx from "rxjs";
// import { catchError, map, mergeMap, switchMap, tap } from "rxjs/operators";
// import { DEFAULT_HTTP_PORT } from "../lib/http/constants";
// import {
//   getExpressApp,
//   getExpressObsevablesList,
//   IObservableExpress,
// } from "../lib/http/express-utils";
// import { IHttpResponse } from "../lib/http/http";
// import { database } from "../lib/repository/database";
// import { IDatabaseSchema } from "../lib/repository/schema";
// import { logger } from "../lib/utils/logger";
// import {
//   // injectDuration,
//   injectError,
//   injectStartTime,
//   processRequest,
//   ITmpWithTime,
// } from "../lib/utils/server";
// import { allSchemas } from "../models";
// import { expressRoutes } from "./routes/user";

// export const getExpressObservable = (
//   app: express.Express,
// ): Rx.Observable<IObservableExpress> => {
//   const expressObservables = getExpressObsevablesList(app, expressRoutes);
//   return Rx.merge(...expressObservables);
// };

// export const loadHttpServer = (app: express.Express, port: number) => {
//   const p = new Promise((resolve) => {
//     http.createServer(app).listen(port, () => {
//       resolve();
//     });
//   });

//   return Rx.from(p);
// };

// interface IProcessedHttpCall {
//   duration: number;
//   payload: IHttpResponse;
//   response: express.Response;
// }

// export const getObservableApplication = (
//   port: number = DEFAULT_HTTP_PORT,
//   schemas: IDatabaseSchema[] = allSchemas,
// ): Rx.Observable<IProcessedHttpCall> => {
//   const expressApp = getExpressApp(port);
//   const expressObservable = getExpressObservable(expressApp);

//   return loadHttpServer(expressApp, port).pipe(
//     tap(() => logger.info(`Express server listening on port ${port}`)),

//     switchMap(() => database.init(schemas)),
//     tap(() => logger.info("Connected to database")),

//     switchMap(() => expressObservable),
//     tap((x) => logger.info(`Processing URL: ${x.url}`)),

//     switchMap(({ url, request, response, endpoint }) => {
//       // Inject start date
//       return Rx.of({ endpoint, request }).pipe(
//         map(injectStartTime),

//         // Switch to a disposable observable
//         switchMap((expressRequestWithTime: ITmpWithTime) => {
//           return Rx.of(expressRequestWithTime).pipe(

//             mergeMap(
//               (reqWithTime: ITmpWithTime) => {
//                 return new Promise<{
//                   payload: IHttpResponse;
//                 }>((resolve, reject) => {
//                   reqWithTime.endpoint(reqWithTime.request)
//                     .then((payload) => {
//                       resolve({
//                         payload,
//                       });
//                     })
//                     .catch((err) => reject(err));
//                 });
//               },

//               // catchError((err, _) => {
//               //   return Rx.of({
//               //     ...expressRequestWithTime,
//               //     payload: {
//               //       message: `${err}`,
//               //       statusCode: 400,
//               //     },
//               //   });
//               // }),
//             ),
//             switchMap((vv) =>
//               Rx.of({ payload: vv.payload, response }),
//             ),
//           );
//         }),
//       );
//     }),

//     tap((x) => {
//       x.response.status(x.payload.statusCode).send(x.payload.message);
//     }),
//     map(injectDuration),
//   );
// };
