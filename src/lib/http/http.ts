export enum HttpMethod {
  GET = "get",
  POST = "post",
  PATCH = "patch",
  DELETE = "delete",
}

export interface IKeyValuePair {
  key: string;
  value: any;
}

export interface IHttpRequest {
  params: IKeyValuePair[];
  metadata: IHttpMetadata;
}

export interface IHttpResponse {
  statusCode: number;
  message: any;
}

export interface IHttpMetadata {
  host: string;
  ip: string;
  userAgent: IHttpUserAgent | undefined;
}

export interface IHttpUserAgent {
  browser: string;
  version: string;
  os: string;
  platform: string;
  source: string;
}
