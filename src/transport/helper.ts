import express from "express";
import http from "http";
import * as Rx from "rxjs";
import { switchMap, tap } from "rxjs/operators";
import { DEFAULT_HTTP_PORT } from "../lib/http/constants";
import { getExpressApp, getExpressObsevablesList, IObservableExpress } from "../lib/http/express-utils";
import { IHttpResponse } from "../lib/http/http";
import { database } from "../lib/repository/database";
import { IDatabaseSchema } from "../lib/repository/schema";
import { logger } from "../lib/utils/logger";
import { allSchemas } from "../models";
import { expressRoutes } from "./routes/user";

export const getExpressObservable = (
  app: express.Express,
): Rx.Observable<IObservableExpress> => {
  const expressObservables = getExpressObsevablesList(app, expressRoutes);
  return Rx.merge(...expressObservables);
};

export const loadHttpServer = (app: express.Express, port: number) => {
  const p = new Promise((resolve) => {
    http.createServer(app).listen(port, () => {
      resolve();
    });
  });

  return Rx.from(p);
};

export interface IProcessedHttpCall {
  duration: number;
  payload: IHttpResponse;
  response: express.Response;
}

export const getHotExpress = (
  port: number = DEFAULT_HTTP_PORT,
  schemas: IDatabaseSchema[] = allSchemas,
): Rx.Observable<IObservableExpress> => {
  const expressApp = getExpressApp(port);
  const expressObservable = getExpressObservable(expressApp);

  return loadHttpServer(expressApp, port).pipe(
    tap(() => logger.info(`Express server listening on port ${port}`)),

    switchMap(() => database.init(schemas)),
    tap(() => logger.info("Connected to database")),

    switchMap(() => expressObservable),
    tap((x) => logger.info(`Processing URL: ${x.url}`)),
  );
};

// url: string;
// endpoint: IEndpointInstance;
// request: IHttpRequest;
// response: express.Response;

// export const getObservableApplication = (
//    source: Rx.Observable<IObservableExpress>,
// ): Rx.Observable<IProcessedHttpCall> => {
//   return source.pipe(
//     map(injectStartTime),
//     switchMap((x) =>
//       Rx.of(x).pipe(
//         mergeMap(processRequest),
//         catchError(injectError),
//       ),
//     ),
//     map(injectDuration),
//     tap((x) => logger.info(x.payload.message)),
//   )};

