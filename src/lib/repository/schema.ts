import sequelize from "sequelize";

export interface IDatabaseSchema {
  name: string;
  options: sequelize.DefineAttributes;
}

export type ISchemaInstance<T> = sequelize.Instance<T> | T;
export type IModel<T> = sequelize.Model<T, ISchemaInstance<T>>;
