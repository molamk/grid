import express from "express";
import * as ExpressUserAgent from "express-useragent";
import { IExpressRoute, IRouteParamLocation } from "../routes/route";
import {
  URL_PARAM_NEGATIVE_POSITION,
  URL_PARAM_SAME_POSITION_ERROR,
  URL_PARAM_WITHOUT_POSITION_ERROR,
} from "./constants";
import {
  IHttpMetadata,
  IHttpRequest,
  IHttpUserAgent,
  IKeyValuePair,
} from "./http";

const NO_POSITION_URL_PARAM = -1;

export const getMetadata = (req: express.Request): IHttpMetadata => ({
  host: req.hostname,
  ip: req.ip,
  userAgent:
    req.useragent === undefined ? undefined : formatUserAgent(req.useragent),
});

const formatUserAgent = (
  userAgent: ExpressUserAgent.UserAgent,
): IHttpUserAgent => ({
  browser: userAgent.browser,
  os: userAgent.os,
  platform: userAgent.platform,
  source: userAgent.source,
  version: userAgent.version,
});

export const formatExpressRequest = (
  req: express.Request,
  route: IExpressRoute,
): IHttpRequest => {
  const metadata: IHttpMetadata = getMetadata(req);
  const params: IKeyValuePair[] = [];
  if (route.params) {
    route.params.forEach((p) => {
      if (req[p.location] && req[p.location][p.key]) {
        params.push({
          key: p.key,
          value: req[p.location][p.key],
        });
      }
    });
  }

  return {
    metadata,
    params,
  };
};

interface ITmpParam {
  key: string;
  position: number;
}

const isParamsWithPositions = (urlParams: ITmpParam[]): boolean =>
  urlParams.findIndex((p) => p.position === NO_POSITION_URL_PARAM) > -1;

const isDuplicatePositionsParams = (urlParams: ITmpParam[]): boolean => {
  const positions = urlParams.map((p) => p.position);
  return positions.length !== [...new Set(positions)].length;
};

export const getFormattedURL = (route: IExpressRoute) => {
  const urlParams = route.params
    ? route.params
        .filter((p) => p.location === IRouteParamLocation.URL)
        .map((x) => ({
          key: x.key,
          position:
            x.position === undefined ? NO_POSITION_URL_PARAM : x.position,
        }))
    : [];

  if (isParamsWithPositions(urlParams)) {
    throw new Error(URL_PARAM_WITHOUT_POSITION_ERROR);
  } else if (isDuplicatePositionsParams(urlParams)) {
    throw new Error(URL_PARAM_SAME_POSITION_ERROR);
  } else if (urlParams.filter((p) => p.position < 0).length > 0) {
    throw new Error(URL_PARAM_NEGATIVE_POSITION);
  } else {
    const orderedParams = urlParams.sort((x, y) => x.position - y.position);
    const slashIfParamsNotEmpty = orderedParams.length > 0 ? "/" : "";
    return (
      route.url +
      slashIfParamsNotEmpty +
      orderedParams.map((p) => `:${p.key}`).join("/")
    );
  }
};
