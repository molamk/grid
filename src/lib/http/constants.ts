import { IHttpResponse } from "./http";

export const DEFAULT_HTTP_PORT = 3000;

export const HTTP_NOT_FOUND_ERROR: IHttpResponse = {
  message: "Not found",
  statusCode: 404,
};

export const URL_PARAM_WITHOUT_POSITION_ERROR =
  "URL params must have a position";

export const URL_PARAM_SAME_POSITION_ERROR =
  "URL params must have different positions";

export const URL_PARAM_NEGATIVE_POSITION =
  "URL params positions must be positive";
