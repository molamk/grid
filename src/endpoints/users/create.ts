import { IEndpointDefinition } from "../../lib/endpoint/endpoint";
import { userModel } from "../../models/user";

export const CreateUserEndpoint: IEndpointDefinition<
  { email: string; password: string },
  { id: number }
> = {
  decoder: (req) => {
    const emailParam = req.params.find((p) => p.key === "email");
    const passwordParam = req.params.find((p) => p.key === "password");
    if (
      emailParam === undefined ||
      typeof emailParam.value !== "string" ||
      passwordParam === undefined ||
      typeof passwordParam.value !== "string"
    ) {
      throw new Error("Could not decode request");
    } else {
      return { email: emailParam.value, password: passwordParam.value };
    }
  },

  encoder: (res) => ({
    message: JSON.stringify({ id: res.id }),
    statusCode: 200,
  }),

  processor: ({ email, password }) =>
    new Promise((resolve, reject) => {
      userModel
        .create({ email, password })
        .then((u) => {
          if (u.id === undefined) {
            reject();
          } else {
            resolve({ id: u.id });
          }
        })
        .catch((err) => reject(err));
    }),
};
