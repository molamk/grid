import { IEndpointDefinition } from "../../lib/endpoint/endpoint";
import { userModel } from "../../models/user";

interface ISecureUser {
  id: number;
  email: string;
}

export const GetAllUsersEndpoint: IEndpointDefinition<
  {},
  { users: ISecureUser[] }
> = {
  decoder: () => ({}),

  encoder: (res) => ({
    message: JSON.stringify(res.users),
    statusCode: 200,
  }),

  processor: () =>
    new Promise((resolve, reject) => {
      userModel
        .findAll()
        .then((users) =>
          resolve({
            users: users
              .filter((u) => u.id !== undefined)
              .map((u) => ({ id: u.id || -1, email: u.email })),
          }),
        )
        .catch((err) => reject(err));
    }),
};
