import * as Rx from "rxjs";
import sequelize from "sequelize";
import { DEFAULT_DB_CONFIG } from "./dbConfig";
import { IDatabaseSchema, ISchemaInstance } from "./schema";

class DatabaseSingleton {
  public static getInstance() {
    if (!DatabaseSingleton.instance) {
      DatabaseSingleton.instance = new DatabaseSingleton();
    }
    return DatabaseSingleton.instance;
  }

  private static instance: DatabaseSingleton;
  private db: sequelize.Sequelize;
  private initialized = false;

  private constructor() {
    this.db = new sequelize(
      DEFAULT_DB_CONFIG.name,
      DEFAULT_DB_CONFIG.username,
      DEFAULT_DB_CONFIG.password,
      {
        dialect: "mysql",
        host: DEFAULT_DB_CONFIG.host,
        logging: false,
        operatorsAliases: false,

        pool: {
          acquire: 30000,
          idle: 10000,
          max: 5,
          min: 0,
        },
      },
    );
  }

  public getDB(): sequelize.Sequelize {
    return this.db;
  }

  public syncAll(schemas: IDatabaseSchema[]) {
    return Promise.all(
      schemas.map((s) => this.getModel(s)).map((s) => s.sync()),
    );
  }

  public getModel<T>(
    schema: IDatabaseSchema,
    hooks?: sequelize.HooksDefineOptions<T>,
  ): sequelize.Model<T, ISchemaInstance<T>> {
    return this.db.define<T, ISchemaInstance<T>>(schema.name, schema.options, {
      hooks,
    });
  }

  public init(schemas: IDatabaseSchema[] = []) {
    const p = new Promise((resolve, reject) => {
      if (!this.initialized) {
        this.db
          .authenticate()
          .then(() => this.syncAll(schemas))
          .then(() => {
            this.initialized = true;
            resolve();
          })
          .catch(() => reject());
      } else {
        resolve();
      }
    });

    return Rx.from(p);
  }
}

export const database = DatabaseSingleton.getInstance();
