import { IHttpMetadata, IHttpRequest, IHttpResponse, IKeyValuePair } from "../http/http";

export const fakeEndpoint = (_: IHttpRequest): Promise<IHttpResponse> =>
  new Promise<IHttpResponse>((resolve) => {
    resolve({
      message: "ok",
      statusCode: 200,
    });
  });

export const fakeKeyValuePairList: IKeyValuePair[] = [];

export const fakeMetadata: IHttpMetadata = {
  host: "",
  ip: "",
  userAgent: undefined,
};

export const fakeHttpRequest: IHttpRequest = {
  metadata: fakeMetadata,
  params: fakeKeyValuePairList,
};

// export const fakeObservableExpress: IObservableExpress = {
//   endpoint: fakeEndpoint,
//   request: fakeHttpRequest,
//   response: p(),
//   url: "",
// };
