import { IExpressRoute, IRouteParamLocation } from "../routes/route";
import { fakeEndpoint as endpoint } from "../testing/fakes";
import { getFormattedURL } from "./builder";
import {
  URL_PARAM_NEGATIVE_POSITION,
  URL_PARAM_SAME_POSITION_ERROR,
  URL_PARAM_WITHOUT_POSITION_ERROR,
} from "./constants";
import { HttpMethod } from "./http";

const BASE_ROUTE: IExpressRoute = {
  endpoint,
  method: HttpMethod.GET,
  url: "/hello",
};

test("it should not alter URLs without parameters", () => {
  const route: IExpressRoute = { ...BASE_ROUTE };
  const formattedURL = getFormattedURL(route);

  expect(formattedURL).toBe("/hello");
});

test("it should reject URL params without position", () => {
  const route: IExpressRoute = {
    ...BASE_ROUTE,
    params: [
      {
        key: "userId",
        location: IRouteParamLocation.URL,
      },
    ],
  };

  expect(() => getFormattedURL(route)).toThrowError(
    URL_PARAM_WITHOUT_POSITION_ERROR,
  );
});

test("it should reject URL params if two or more have the same position", () => {
  const route: IExpressRoute = {
    ...BASE_ROUTE,
    params: [
      {
        key: "userId",
        location: IRouteParamLocation.URL,
        position: 0,
      },
      {
        key: "name",
        location: IRouteParamLocation.URL,
        position: 0,
      },
    ],
  };

  expect(() => getFormattedURL(route)).toThrowError(
    URL_PARAM_SAME_POSITION_ERROR,
  );
});

test("it should reject URL params with negative positions", () => {
  const route: IExpressRoute = {
    ...BASE_ROUTE,
    params: [
      {
        key: "userId",
        location: IRouteParamLocation.URL,
        position: -10,
      },
    ],
  };

  expect(() => getFormattedURL(route)).toThrowError(
    URL_PARAM_NEGATIVE_POSITION,
  );
});

test("it should alter URLs with parameters", () => {
  const route: IExpressRoute = {
    ...BASE_ROUTE,
    params: [
      {
        key: "userId",
        location: IRouteParamLocation.URL,
        position: 0,
      },
    ],
  };
  const formattedURL = getFormattedURL(route);

  expect(formattedURL).toBe("/hello/:userId");
});

test("it should put URL params in the correct order", () => {
  const route: IExpressRoute = {
    ...BASE_ROUTE,
    params: [
      {
        key: "userId",
        location: IRouteParamLocation.URL,
        position: 0,
      },
      {
        key: "name",
        location: IRouteParamLocation.URL,
        position: 1,
      },
    ],
  };
  const formattedURL = getFormattedURL(route);

  expect(formattedURL).toBe("/hello/:userId/:name");
});
