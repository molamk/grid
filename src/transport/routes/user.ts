import { CreateUserEndpoint } from "../../endpoints/users/create";
import { GetAllUsersEndpoint } from "../../endpoints/users/getAll";
import { generateEndpoint } from "../../lib/endpoint/endpoint";
import { HttpMethod } from "../../lib/http/http";
import { IExpressRoute, IRouteParamLocation } from "../../lib/routes/route";

export const expressRoutes: IExpressRoute[] = [
  {
    endpoint: generateEndpoint(GetAllUsersEndpoint),
    method: HttpMethod.GET,
    url: "/users",
  },
  {
    endpoint: generateEndpoint(CreateUserEndpoint),
    method: HttpMethod.POST,
    params: [
      {
        key: "email",
        location: IRouteParamLocation.BODY,
      },
      {
        key: "password",
        location: IRouteParamLocation.BODY,
      },
    ],
    url: "/users",
  },
];
