import { IEndpointInstance } from "../endpoint/endpoint";
import { HttpMethod } from "../http/http";

export enum IRouteParamLocation {
  URL = "params",
  QUERY = "query",
  BODY = "body",
}

export interface IRouteParam {
  key: string;
  location: IRouteParamLocation;
  position?: number;
}

export interface IExpressRoute {
  url: string;
  method: HttpMethod;
  params?: IRouteParam[];
  endpoint: IEndpointInstance;
}
